# Graphcool backend for Pubsweet

## Set up

1. `yarn`
2. `npx graphcool local up` to start a local cluster
3. `npx graphcool deploy` and a cluster to deploy to
4. Copy the **Simple API** endpoint and copy it to the `API_ENDPOINT` config in `pubsweet-starter`
